from cmath import e
from logging import root
import requests
import os
import xml.etree.ElementTree as ET

URL_LIST_FILE = os.environ['URL_LIST_FILE']
SITEMAP_URL = os.environ['SITEMAP_URL']


def parse_index(root):
    for element in root.iterfind('{*}sitemap/{*}loc'):
        yield from parse_url(element.text)

def parse_urlset(root):
    for element in root.iterfind('{*}url/{*}loc'):
        if element.text is not None:  # Why does this sometimes return None?
            print(f'Discovered {element.text}')
            yield(element.text)
            yield('\n')

def parse_url(url):
    try:
        resp = requests.get(url, stream=True)
        resp.raw.decode_content = True
        _, root = next(ET.iterparse(resp.raw, events=['start']))
        if root.tag.endswith('urlset'):
            print(f'Parsing {url} as index')
            yield from parse_urlset(root)
        elif root.tag.endswith('sitemapindex'):
            print(f'Parsing {url} as sitemap')
            yield from parse_index(root)
    except ET.ParseError:
        print(f'Error parsing {url}: {e}')

with open(URL_LIST_FILE, 'w') as f:
    f.writelines(
        parse_url(SITEMAP_URL)
    )