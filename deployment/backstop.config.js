// ref: https://github.com/garris/BackstopJS#working-with-your-config-file
const fs = require('fs');
const url_file = `${process.env.URL_LIST_FILE}`

String.prototype.slugify = function () {
    return this
        .replace(`https://${process.env.ENVIRONMENT_URL}/`, '')
        .normalize('NFD')                 // split an accented letter in the base letter and the accent
        .replace(/[\u0300-\u036f]/g, '')  // remove all previously split accents
        .replace(/[^a-z0-9_-]/g, '-')     // remove all chars not letters, numbers and spaces (to be replaced)
        .replace(/-+/g, '-');             // collapse contiguous replacements
};

function urls() {
    let file_content = fs.readFileSync(url_file)

    if (!file_content.length) {
        console.log("URLs file was empty. Falling back to $ENVIRONMENT_URL only\n")
        return new Array(`https://${process.env.ENVIRONMENT_URL}`)
    }

    return file_content.toString().trim().split("\n")
}

module.exports = {
    "id": `${process.env.PROJECT_SLUG}`,
    "viewports": [
        {
            "label": "phone",
            "width": 320,
            "height": 480
        },
        {
            "label": "tablet",
            "width": 1024,
            "height": 768
        }
    ],
    "onBeforeScript": "puppet/onBefore.js",
    "onReadyScript": "puppet/onReady.js",
    "scenarios": urls().map(url => ({
        label: url.slugify(),
        cookiePath: "backstop_data/engine_scripts/cookies.json",
        url: url,
        referenceUrl: "",
        readyEvent: "",
        readySelector: "",
        delay: 0,
        hideSelectors: [],
        removeSelectors: [],
        hoverSelector: "",
        clickSelector: "",
        postInteractionWait: 0,
        selectors: ["document"],
        selectorExpansion: true,
        expect: 0,
        misMatchThreshold: 0.1,
        requireSameDimensions: true
    })),
    // we change the report directories so that our template will pick them up by default
    // test and reference are in different directories so that needs: project can pull
    // the reference images from the main branch job to be used for comparison. As a bonus,
    // the default .reports/**/* pattern picks the reference images up again so that they're
    // saved as artifacts along with the job so all comparison sources are preserved 
    "paths": {
        "bitmaps_reference": ".reports/reference_images",
        "bitmaps_test": ".reports/images",
        "engine_scripts": "backstop_data/engine_scripts",
        "html_report": ".reports/html",
        "ci_report": ".reports/junit"
    },
    "report": [
        "CI"
    ],
    // changing the report filenames to match convention just to make finding them
    // progamatically easier if we ever have to do that
    "ci": {
        "format": "junit",
        "testReportFileName": "gl-backstopjs-xunit-report.xml",
        "testSuiteName": "backstopJS"
    },
    "engine": "puppeteer",
    "engineOptions": {
        "args": ["--no-sandbox"]
    },
    "asyncCaptureLimit": 5,
    "asyncCompareLimit": 50,
    "debug": true,
    "debugWindow": false
}