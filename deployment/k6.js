import http from "k6/http";
import { group, check } from "k6";
import { SharedArray } from 'k6/data';

const urls = new SharedArray('url list', function () {
    let discovered_urls = open(__ENV.URL_LIST_FILE).trim().split('\n')
    if (!discovered_urls.length) {
        console.log("URLs file was empty. Falling back to $CI_ENVIRONMENT_URL only")
        discovered_urls.push(`https://${__ENV.CI_ENVIRONMENT_URL}`)
    }
    return discovered_urls
})

// Helper functions
const isOK = {
    'response code is 200': response => response.status == 200
}

// K6 interface
export const options = {
    tags: {
        does_template: 'true',
        discovered_from_sitemap: 'true',
        sitemap_url: __ENV.SITEMAP_URL
    },
    vus: 10,
    duration: '10s',
    thresholds: {
        http_req_duration: ['p(99)<1500'], // 99% of requests must complete below 1.5s
    },
};

export default function () {

    group('Smoke Test', function () {
        urls.forEach(url => {
            let response = http.get(url);
            check(response, isOK)
        })
    });
};